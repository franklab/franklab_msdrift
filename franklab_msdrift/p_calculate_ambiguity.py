import itertools as it
import os
from logging import getLogger

import numpy as np
from scipy.special import comb
from scipy.sparse import csc_matrix
from scipy.stats import mode

from pyms.basic.p_extract_clips import extract_clips_helper
from pyms.mlpy import DiskReadMda, readmda, writemda32, writemda64
from ml_ms4alg import ms4alg
import isosplit5
import json
import gc
gc.collect()

import franklab_msdrift.p_anneal_segments as ann

def ambiguity_metrics(pairs_to_merge_PW, dmatrix, k1_dmatrix, k2_dmatrix,Kmaxes, segment_combos, excludes={}):
    # excludes: dict, key is epoch index (zero based), values are labels of neurons to be excluded.
    
    distance_within_all=np.array([])
    distance_inter_all=np.array([])
    distance_other_min_all=np.array([])
    pairs_low_confidence_all=[]
    pairs_low_confidence_all_=[]


    for dframe in range(np.shape(dmatrix)[2]):

        # A. How is the 2nd min different from the min
        dmatrix0=dmatrix[:, :, dframe]

        adj_Kmaxes = np.cumsum(Kmaxes)
        if segment_combos[dframe, 0] == 0:
            f1_adj = 0
        else:
            # otherwise increment cluster numbers based on max of previous
            # segment
            f1_adj = int(adj_Kmaxes[segment_combos[dframe, 0] - 1])
        f2_adj = int(adj_Kmaxes[segment_combos[dframe, 1] - 1])

        # get pairs_to_merge
        if len(pairs_to_merge_PW[dframe])>0:
            pairs_to_merge=pairs_to_merge_PW[dframe]-np.reshape(np.array([f1_adj+1,f2_adj+1]),(1,2))
        else:
            continue

        # get pairs_non_merge
        N_f1, N_f2=np.shape(dmatrix[:, :, dframe])
        pairs_matrix=csc_matrix((np.ones(np.shape(pairs_to_merge)[0]),(pairs_to_merge[:,0],pairs_to_merge[:,1])),shape=[N_f1,N_f2])
        pairs_matrix=csc_matrix.todense(pairs_matrix)
        pairs_non_merge=[p for p in np.argwhere(1-pairs_matrix) if np.any(np.isin(pairs_to_merge[:,0],p[0]))]
        #pairs_non_merge=[p for p in np.argwhere(1-pairs_matrix)]
        pairs_non_merge=np.vstack(pairs_non_merge)

        # exclude neurons
        isin_idx_1=np.isin(np.array(list(excludes.keys())),segment_combos[dframe,0])
        if np.any(isin_idx_1):
            exclude_row_idx=np.isin(pairs_non_merge[:,0],np.array(excludes[segment_combos[dframe,0]]))
            pairs_non_merge=pairs_non_merge[~exclude_row_idx,:]
        
        isin_idx_2=np.isin(np.array(list(excludes.keys())),segment_combos[dframe,1])
        if np.any(isin_idx_2):
            exclude_row_idx=np.isin(pairs_non_merge[:,1],np.array(excludes[segment_combos[dframe,1]]))
            pairs_non_merge=pairs_non_merge[~exclude_row_idx,:]
        
        normalized_distance=dmatrix0/(np.nanmax(dmatrix0,axis=1).reshape((-1,1))+0.0000001) #add epislon in case 0
        other_min=normalized_distance[pairs_non_merge[:,0],pairs_non_merge[:,1]]
        other_min=other_min[~np.isnan(other_min)]

        # B. Within-group and between-group

        k1_dmatrix0=k1_dmatrix[:, :, dframe]
        k2_dmatrix0=k2_dmatrix[:, :, dframe]
        normalized_distance=dmatrix0/(k1_dmatrix0+0.0000001) #add epislon in case of 0
        normalized_distance_2=dmatrix0/(k2_dmatrix0+0.0000001) #add epislon in case of 0

        # get distance_within, distance_inter,
        distance_within=normalized_distance[pairs_to_merge[:,0],pairs_to_merge[:,1]]
        distance_inter=normalized_distance[pairs_non_merge[:,0],pairs_non_merge[:,1]]
        distance_inter=distance_inter[~np.isnan(distance_inter)]
    
        # get pairs_low_confidence
        cut_off=1 
        pairs_low_confidence=np.array([p for p in np.argwhere(pairs_matrix) if (np.sum(normalized_distance[p[0],:]<cut_off)>1 or np.sum(normalized_distance_2[:,p[1]]<cut_off)>1)])
        '''  
        for p in np.argwhere(pairs_matrix):
            if np.sum(normalized_distance[p[0],:]<cut_off)>1:# or np.sum(normalized_distance[:,p[1]]<0.194)>1):
                cur_group=np.concatenate((np.argwhere(normalized_distance[p[0],:]<cut_off).reshape((1,-1))[0]+f2_adj+1,[p[0]+f1_adj+1]))
                loc=np.isin(allk[1,:],cur_group)
                cur_group_label=np.unique(allk[2,loc]) #all labels within the location
                loc_all=np.isin(allk[2,:],cur_group_label)#expand to these locations
                allk[2,loc_all]=np.min(allk[2,loc_all])
                
            if np.sum(normalized_distance[:,p[1]]<cut_off)>1:
                cur_group=np.concatenate((np.argwhere(normalized_distance[:,p[1]]<cut_off).reshape((1,-1))[0]+f1_adj+1,[p[1]+f2_adj+1]))
                loc=np.isin(allk[1,:],cur_group)
                cur_group_label=np.unique(allk[2,loc]) #all labels within the location
                loc_all=np.isin(allk[2,:],cur_group_label)#expand to these locations
                allk[2,loc_all]=np.min(allk[2,loc_all])
        '''
            
        if len(pairs_low_confidence)>0:
            pairs_low_confidence[:,0]+= + 1
            pairs_low_confidence[:,1]+= + 1
            pairs_low_confidence_all_.append(pairs_low_confidence.copy())
            pairs_low_confidence[:,0]+=f1_adj
            pairs_low_confidence[:,1]+=f2_adj
            pairs_low_confidence_all.append(pairs_low_confidence)
        
        # tally
        distance_within_all=np.concatenate((distance_within_all,distance_within))
        distance_inter_all=np.concatenate((distance_inter_all,distance_inter))
        distance_other_min_all=np.concatenate((distance_other_min_all,other_min))
    
    try:
        pairs_low_confidence_all=np.vstack(pairs_low_confidence_all)
        pairs_low_confidence_all_=np.vstack(pairs_low_confidence_all_)
    except:
        pairs_low_confidence_all=[]
        pairs_low_confidence_all_=[]


    return (distance_other_min_all,distance_within_all,distance_inter_all,
        pairs_low_confidence_all,pairs_low_confidence_all_)





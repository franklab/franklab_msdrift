import json
import pathlib
import os
from pyms.basic.p_extract_clips import extract_clips_helper
from pyms.basic.p_extract_timeseries import extract_timeseries
from pyms.mlpy import DiskReadMda, readmda, writemda32, writemda64
import numpy as np
import multiprocessing
import functools


from sklearn.decomposition import PCA
from sklearn.neighbors import KDTree


def metrics_isolation(timeseries,firing,clip_size,output_folder='',output_name=''):
    (times,labels,waveforms,templates,units)=times_labels_templates(timeseries,firing,clip_size=clip_size)
    pairs_to_compare=get_pairs_to_compare(labels,units,templates)

    isolation_score=[]
    for i in range(len(units)):
        k1=units[i]
        k1_pairs=pairs_to_compare[i]
        score=0 #initialize
        pair=0
        for j in k1_pairs:
            score_,_=computer_overlap(waveforms[i],waveforms[j],opts={})
            if score_>score:
                score=score_
                pair=units[j]
        isolation_score_={}
        isolation_score_['label']=int(k1)
        isolation_score_metrics={}
        isolation_score_metrics['isolation']=float(1-score)
        isolation_score_metrics['overlap_cluster']=int(pair)
        isolation_score_['metrics']=isolation_score_metrics
        isolation_score.append(isolation_score_) 

    if len(output_folder)==0:
        return isolation_score

    # write file
    metrics_output={}
    metrics_output['clusters']=isolation_score

    metrics_file = os.path.join(output_folder,output_name)
    with open(metrics_file, 'w') as f:
        json.dump(metrics_output, f, indent=4, sort_keys=True)
    return isolation_score

def computer_overlap(waveform1,waveform2,opts={}):
    k1=1
    k2=2
    if len(opts)==0:
        opts['k_nearest_num']=6
        
    times1=np.arange(np.shape(waveform1)[1])
    times2=np.arange(np.shape(waveform2)[1])
    num_samples=np.min([len(times1),len(times2),500])
    times1_sub=np.random.choice(times1,num_samples,replace=False)
    times2_sub=np.random.choice(times2,num_samples,replace=False)
    
    all_times=np.concatenate((times1_sub,times2_sub))
    all_labels=np.concatenate((np.ones(len(times1_sub))*k1,np.ones(len(times2_sub))*k2))
    all_clips_reshaped=np.concatenate((waveform1[:,times1_sub],waveform2[:,times2_sub]),axis=1)

    pca = PCA(n_components=10)
    FF=pca.fit_transform(all_clips_reshaped.T) #FF: number of example x PCA n_components
    tree = KDTree(FF, leaf_size=2)  
    k_nearest_num=opts['k_nearest_num']
    dist, ind = tree.query(FF, k=k_nearest_num) 
    
    result=all_labels[ind]==all_labels.reshape((-1,1)).dot(np.ones((1,k_nearest_num))) #correct
    return 1-np.mean(np.mean(result,axis=0)),all_labels[ind]

def get_waveforms_templates(unit,timeseries,times,labels):
    times1=times[labels==unit]
    num_samples=np.min([len(times1),500])
    times1_sub=np.random.choice(times1,num_samples,replace=False)
    clips1=extract_clips_helper(timeseries=timeseries, times=times1_sub, clip_size=50)
    waveform=clips1.reshape((-1,len(times1_sub)))
    template = np.mean(clips1, axis=2) # (# of channels) x T
    return waveform,template,unit

def times_labels_templates(timeseries,firing,clip_size=50,num_worker=12):
    T=DiskReadMda(timeseries)
    F=readmda(firing)
    times=F[1,:]
    labels=F[2,:]

    a=int(np.floor((clip_size+1)/2-1))
    t_start=(times-a).astype('int')
    t_end=(times-a+clip_size).astype('int')
    times=times[(t_start>=0)*(t_end<=T.N2())]
    labels=labels[(t_start>=0)*(t_end<=T.N2())].astype('int')

    max_k=np.max(labels)
    
    pool = multiprocessing.Pool(num_worker)
    helper=functools.partial(get_waveforms_templates,timeseries=timeseries,times=times,labels=labels)
    
    #templates=np.zeros((T.N1(),clip_size,max_k))
    #waveforms={}
    waveforms, templates,units = zip(*pool.map(helper, np.unique(labels)))
    pool.close()
    pool.join()
    
    return times,labels,waveforms,templates,units

def compute_distance_between_templates(centroid1, centroid2, new_flag=True):

    dist1 = np.sum((centroid2 - centroid1) ** 2)
    
    if not new_flag:
        return dist1

    centroid1_= np.mean(centroid1, axis=0)
    centroid2_= np.mean(centroid2, axis=0)
    centroid1_final=centroid1_/np.sqrt(np.sum(centroid1_**2))
    centroid2_final=centroid2_/np.sqrt(np.sum(centroid2_**2))
    
    c=np.correlate(centroid1_final,centroid2_final,"full")
    zero_lag_ind=int((len(c)+1)/2-1) #be careful of the zero indexing
    max_allow=int(np.floor(1/15*len(centroid2_)))
    c=c[(zero_lag_ind-max_allow):(zero_lag_ind+max_allow+1)]
    zero_lag_ind=int((len(c)+1)/2-1)
    ind_max=int(np.argmax(c)-zero_lag_ind)
    centroid2_shifted=np.roll(centroid2,int(ind_max),axis=1)
    dist2 = np.sum((centroid2_shifted - centroid1) ** 2)
    max_corr=np.max(c)
    #print('dis2',dist2)

    return np.min([dist1,dist2]),max_corr

def get_pairs_to_compare(labels,units,templates):
    dist_rms=np.zeros((len(units),len(units)))+np.nan
    dist_corr=np.zeros((len(units),len(units)))+np.nan
    pairs_to_compare={}
    for i in range(len(units)):
        k1=units[i]
        for j in range(len(units)):
            k2=units[j]
            centroid1=templates[i]
            centroid2=templates[j]
            dist_rms[i,j],dist_corr[i,j]=compute_distance_between_templates(centroid1, centroid2, new_flag=True)
        dist_rms_=dist_rms[i,:]
        sorted_units_1=np.argsort(dist_rms_[~np.isnan(dist_rms_)])
        sorted_1=sorted_units_1[:10]
        sorted_2=np.argwhere(dist_corr[i,:]>0.8)[:,0]
        pairs_to_compare[i]=np.array(list(set(list(np.concatenate((sorted_1,sorted_2))))-set([i])))
    return pairs_to_compare
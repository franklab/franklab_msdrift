import itertools as it
import os
import json
from logging import getLogger
import numpy as np
from scipy.special import comb
from scipy.sparse import csc_matrix
from pyms.mlpy import DiskReadMda, readmda, writemda32, writemda64

def track(timeseries_list, firings_list, hippo_options={}):

    (dmatrix, k1_dmatrix, segment_combos, all_waveforms_first, all_waveforms_last, 
        all_K, noise_units,merged_pairs_units,MUA_units)=get_dmatrix(timeseries_list, firings_list, 
                    hippo_options=hippo_options)

    all_K_old=[list(all_waveforms_first[i].keys()) for i in range(len(all_waveforms_first))]
    Kmaxes=[np.max(all_K_old[i]) for i in range(len(all_K_old))]

    pairs_to_merge=get_join_matrix(1-dmatrix,Kmaxes,segment_combos)

    return (pairs_to_merge, dmatrix, k1_dmatrix, segment_combos, all_waveforms_first, all_waveforms_last, 
        all_K, noise_units,merged_pairs_units,MUA_units)

def chain_neuron(firings_list,pairs_to_merge,debug=False):

    ## Make a sequence of labels
    # It is a matrix of 4 rows.
    # (1st row) file number ID.
    # (2nd row) unit ID within that file.
    # (3rd row) unit ID within the whole day, called absolute ID.
    # (4th row) nan. nan will be filled with lowest absolute ID for each "neuron chain".

    adj=0
    allk=[]
    for firings in range(len(firings_list)):
        X=readmda(firings_list[firings])
        allk_=np.array([np.unique(X[2,:]),np.unique(X[2,:])+adj])
        allk_list=np.ones((1,np.shape(allk_)[1]))*(firings+1)
        allk_=np.vstack((allk_list,allk_))
        if len(allk)>0:
            allk=np.concatenate((allk,allk_),axis=1)
        else:
            allk=allk_
        adj+=np.max(np.unique(X[2,:]))
    allk_labels=np.zeros((1,np.shape(allk)[1]))+np.nan
    allk=np.concatenate((allk,allk_labels),axis=0)

    # This section does all the tracking result consolidation: 
    #   fill nan's with the lowest absolute ID for each "neuron chain".
    for cur_group in pairs_to_merge:
        loc=np.isin(allk[2,:],cur_group)
        cur_group_label=np.unique(allk[3,loc]) #all labels within the location
        if len(cur_group_label[~np.isnan(cur_group_label)])==0:#new entry
            allk[3,loc]=np.min(allk[2,loc])
        else:
            loc_=np.isin(allk[3,:],cur_group_label[~np.isnan(cur_group_label)]) #expand to these locations
            loc_all=np.hstack((np.argwhere(loc_).ravel(),np.argwhere(loc).ravel()))
            if not np.sum(np.diff(np.sort(allk[0,loc_all]))==0)>1: # no conflict
                allk[3,loc_all]=np.nanmin(allk[3,loc_all])
            
    # Make the output table
    alllabels=np.unique(allk[3,~np.isnan(allk[3,:])]).astype('int')
    allk=allk.astype('int')
    groups=np.zeros((np.max(alllabels),np.max(np.unique(allk[0,:]))))
    groups_within=np.zeros_like(groups)
    
    del_ind=np.zeros(np.max(alllabels))>0
    epochs=np.arange(np.max(np.unique(allk[0,:])))+1

    for l in alllabels:
        groups_idx=allk[3,:]==l
        groups_l=allk[:,groups_idx]
        #if np.any(np.diff(allk[0,groups_idx])==0) or debug: #single epoch shows up twice
        #    groups_unsure[l]=groups_l[:3,:]
        #    del_ind[l-1]=True
        #else:
        groups[l-1,np.isin(epochs,groups_l[0,:])]=groups_l[2,:]
        groups_within[l-1,np.isin(epochs,groups_l[0,:])]=groups_l[1,:]
    #groups_sure=groups_sure[~del_ind,:]
    groups_part=groups[np.sum(groups,axis=1)>0,:]
    groups_within_part=groups_within[np.sum(groups,axis=1)>0,:]
    return groups,groups_within,groups_part,groups_within_part

def get_dmatrix(timeseries_list, firings_list, hippo_options={}):

    num_segments = len(timeseries_list)
    # Get all possible segment combinations
    segment_combos = it.combinations(range(num_segments), 2)
    segment_combos = np.array(list(segment_combos))
    # Order segment combinations such that neighbors are first,
    # then non-neighbors
    segment_combos = np.append(
        segment_combos[np.where(np.diff(segment_combos) == 1)[0], :],
        segment_combos[np.where(np.diff(segment_combos) > 1)[0], :], axis=0)

    all_waveforms_first,all_waveforms_last=get_clips(timeseries_list,firings_list,clip_size=50)
    all_K=[list(all_waveforms_first[i].keys()) for i in range(len(all_waveforms_first))]
    
    if len(hippo_options)>0: #do hippocampus data annealing
        all_waveforms_first,all_waveforms_last, all_K, noise_units,merged_pairs_units,MUA_units=update(all_waveforms_first,all_waveforms_last,
            hippo_options['metrics_file_folder'],hippo_options['metrics_file_name'])

    dmatrix,_,k1_dmatrix=calculate_dmatrix(all_waveforms_first,all_waveforms_last,
        all_K, segment_combos,sample_num=10000)

    return (dmatrix, k1_dmatrix, segment_combos, all_waveforms_first, all_waveforms_last, 
                all_K, noise_units,merged_pairs_units,MUA_units)

def calculate_dmatrix(all_waveforms_first,all_waveforms_last,all_K,segment_combos,sample_num=10000):
    half_sample_num=int(sample_num/2)
    template_int=1

    if len(all_K)==0:
        all_K=[np.array(list(all_waveforms_first[i].keys())) for i in range(len(all_waveforms_first))]
    Kmaxes=[np.max(all_K[i]) for i in range(len(all_K))]

    overlap_area=np.zeros((int(max(Kmaxes)),int(max(Kmaxes)),np.shape(segment_combos)[0]))+np.nan
    corr_coef=np.zeros((int(max(Kmaxes)),int(max(Kmaxes)),np.shape(segment_combos)[0]))+np.nan
    variance_within_1=np.zeros((int(max(Kmaxes)),int(max(Kmaxes)),np.shape(segment_combos)[0]))+np.nan


    for dframe in range(np.shape(segment_combos)[0]):
        # count up to number of combinations for dmatrix 3rd dimension indexing
        j1 = segment_combos[dframe, 0]
        j2 = segment_combos[dframe, 1]
        K1=all_K[j1]
        K2=all_K[j2]
        if (len(K1) == 0 or len(K2) == 0):
            continue
        for x1 in K1:
            data1=all_waveforms_last[j1][x1]
            C,T,N1=np.shape(data1)
            n1=np.random.choice(N1,sample_num,replace=True)

            dist11=np.zeros((int(C),int(half_sample_num/template_int)))
            for t in range(0,half_sample_num-template_int,template_int):
                data1_=np.squeeze(data1[:,:,n1[(t):(t+template_int)]])
                data2_=np.squeeze(data1[:,:,n1[(half_sample_num+t):(half_sample_num+t+template_int)]])
                dist11[:,int(t/template_int)],_,__=compute_distance_between_clusters_2(data1_,data2_)
            for x2 in K2:
                data2=all_waveforms_first[j2][x2]
                C,T,N2=np.shape(data2)
                n2=np.random.choice(N2,sample_num,replace=1)
            
            #dist11=np.sum((data1[:,n1[:half_sample_num]]-data1[:,n1[half_sample_num:]])**2,axis=0)

            # correlation coef
            # dist11=get_corrcoef(data1[:,n1[:half_sample_num]],data1[:,n1[half_sample_num:]])
            # last resort
                
                #dist22=np.zeros((int(C),int(half_sample_num/template_int)))
                dist12=np.zeros((int(C),int(half_sample_num/template_int)))

                for t in range(0,half_sample_num-template_int,template_int):

                    data1_=np.squeeze(data1[:,:,n1[(t):(t+template_int)]])
                    data2_=np.squeeze(data2[:,:,n2[(t):(t+template_int)]])       
                    dist12[:,int(t/template_int)],_,__=compute_distance_between_clusters_2(data1_,data2_)

                    #data1_=data2[:,:,n2[(half_sample_num+t):(half_sample_num+t+template_int)]]
                    #dist22[:,int(t/template_int)],_,__=compute_distance_between_clusters_2(data1_,data2_) #np.max(np.sqrt(np.sum((data1_-data2_)**2,axis=1)),axis=0)
                
                overlap_temp=np.zeros(int(C))+np.nan
                corr_temp=np.zeros(int(C))+np.nan
                for dim in range(int(C)):
                    min_plt=np.quantile(dist11[dim,:],0.01)
                    max_plt=np.quantile(dist12[dim,:],0.99)
                    hist_11=np.histogram(dist11[dim,:],bins=1000,range=(min_plt,max_plt))
                    hist_12=np.histogram(dist12[dim,:],bins=1000,range=(min_plt,max_plt))
                    union_area=np.sum(np.max(np.vstack((hist_11[0].reshape((1,-1)),hist_12[0].reshape((1,-1)))),axis=0))
                    overlap_temp[dim]=(np.sum(hist_11[0])+np.sum(hist_12[0])-union_area)/(union_area+0.000001)
                    corr_temp[dim]=np.dot(hist_11[0],hist_12[0])/(np.sqrt(np.sum(hist_11[0]**2))*np.sqrt(np.sum(hist_12[0]**2)))#np.corrcoef(hist_11[0],hist_12[0])[0,1]
                overlap_area[x1-1,x2-1,dframe]=np.min(overlap_temp)
                corr_coef[x1-1,x2-1,dframe]=np.min(corr_temp)

                dim=np.argmin(overlap_temp)
                variance_within_1[x1-1,x2-1,dframe]=np.var(dist11[dim])
                #variance_within_2[x1-1,x2-1,dframe]=np.var(dist22[dim])
 
    return overlap_area,corr_coef,variance_within_1 #,variance_within_2

def update(all_waveforms_first,all_waveforms_last,metrics_file_folder,metrics_file_name):
    # This function assumes some folder structure

    # metrics_file_name: 'metrics_processed_nt'+electrodestr+'_epoch'+str(epoch+1)+'.json'
    # metrics_file_folder=os.path.join(mountainout_folder,'metrics')

    all_K_updated=[]
    merged_pairs_all=[]
    noise_all=[]

    MUA_all=find_MUA(all_waveforms_first,all_waveforms_last)


    for epoch in range(len(all_waveforms_first)):

        '''
        Get metrics file paths
        '''

        K_=list(all_waveforms_first[epoch].keys())
        print('K_',K_)
        metrics_file=os.path.join(metrics_file_folder,metrics_file_name+'_epoch'+str(epoch+1)+'.json')
        metrics_file_updated=os.path.join(metrics_file_folder,metrics_file_name+'_epoch'+str(epoch+1)+'_updated.json')
        
        '''
        1. Remove noise
        '''
        noise_units=find_noise_units(metrics_file)
        noise_all.append(noise_units)
        K_=list(set(K_.copy())-set(noise_units))
        print('K_ noise removed',K_)

        '''
        2. Remove MUA
        '''
        MUA_units=MUA_all[epoch]  
        K_=list(set(K_.copy())-set(MUA_units.tolist()))

#        exclude_units=np.concatenate((bursting_units,merged_pairs.ravel()))
#        MUA_units=find_MUA_units(metrics_file,metrics_file_updated,exclude_units)
#        MUA_all.append(MUA_units)

        '''
        3.(1) Esitimate bursting pairs
        '''
        bursting_pairs=find_bursting_pairs(metrics_file)

        '''
        4. Merge within epoch
        '''
        # the function updates K_
        all_waveforms_first[epoch],all_waveforms_last[epoch],K_,merged_pairs=merge_within_epoch(
            all_waveforms_first[epoch],all_waveforms_last[epoch],K_,metrics_file_updated,bursting_pairs)
        merged_pairs_all.append(merged_pairs)

        '''
        3.(2) Remove bursting parents, only keep bursting child
        '''
        if len(bursting_pairs)>0:
            K_=list(set(K_.copy())-set(bursting_pairs[:,0].tolist()))
        print('K_final',K_)

        all_K_updated.append(K_)

    return all_waveforms_first,all_waveforms_last, all_K_updated, noise_all,merged_pairs_all,MUA_all


def interp2D(x,fold_increase):
    t=np.arange(0,np.shape(x)[1])
    t_inq=np.arange(0,np.shape(x)[1],1/fold_increase)
    x_inq=[]
    for dim1 in range(np.shape(x)[0]):
        x_inq.append(np.interp(t_inq,t,x[dim1,:]))
    return np.array(x_inq)

def compute_distance_between_clusters_2(clips1, clips2, interp_degree=4):
    #centroid1 = interp2D(np.mean(clips1, axis=2),4)
    #centroid2 = interp2D(np.mean(clips2, axis=2),4)
    #dist1 = np.sum((centroid2 - centroid1) ** 2,axis=1)

    centroid1 = interp2D(clips1,4)
    centroid2 = interp2D(clips2,4)

    centroid1_= np.reshape(centroid1,(1,-1)).ravel()
    centroid2_= np.reshape(centroid2,(1,-1)).ravel()
    centroid1_= centroid1_/np.max(centroid1_)
    centroid2_= centroid2_/np.max(centroid2_)

    c=np.correlate(centroid1_,centroid2_,"full")
    zero_lag_ind=int((len(c)+1)/2-1) #be careful of the zero indexing
    max_allow=int(np.floor(1/15*len(centroid2_)))
    c=c[(zero_lag_ind-max_allow):(zero_lag_ind+max_allow+1)]
    zero_lag_ind=int((len(c)+1)/2-1)
    ind_max=int(np.argmax(c)-zero_lag_ind)
    centroid2_shifted=np.roll(centroid2,int(ind_max),axis=1)

    centroid2_shifted_=np.reshape(centroid2_shifted,(1,-1)).ravel()
    centroid2_shifted_= centroid2_shifted_/np.max(centroid2_shifted_)


    dist2 = np.sum((centroid2_shifted/np.max(centroid2_) - centroid1/np.max(centroid1_)) ** 2,axis=1)
    #print('dis2',dist2)

    return dist2,centroid1_,centroid2_shifted_ #np.min([dist1,dist2])



def get_clips(timeseries,firings,clip_size=50):
    all_waveforms_first = []
    all_waveforms_last = [] 
    #example: all_waveforms[0][2]: the waveforms for the first epoch's neuron 2, (waveforms of # of channels concatenated) x (# of examples)

    for i in range(len(timeseries)): # loop over files
        timeseries1=timeseries[i]
        firings1=firings[i]
        all1={}
        all2={}

        X = DiskReadMda(timeseries1)
        F=readmda(firings1)
        M = X.N1()

        F1 = get_first_events(F, 250)
        times1 = F1[1, :].ravel()
        labels1 = F1[2, :].ravel()
        clips1,del_ind = extract_clips_helper2(
            timeseries=timeseries1, times=times1, clip_size=clip_size)
        labels1=labels1[~del_ind]

        F2 = get_last_events(F, 250)
        times2 = F2[1, :].ravel()
        labels2 = F2[2, :].ravel()
        clips2,del_ind = extract_clips_helper2(
            timeseries=timeseries1, times=times2, clip_size=clip_size)
        labels2=labels2[~del_ind]

        K1 = np.unique(labels1).astype('int')
        for k1 in K1: # loop over units
            inds_k1 = np.where(labels1 == k1)[0]
            all1[k1] = clips1[:, :, inds_k1]

            inds_k2 = np.where(labels2 == k1)[0]
            all2[k1] = clips2[:, :, inds_k2]

        all_waveforms_first.append(all1)
        all_waveforms_last.append(all2)

    return all_waveforms_first,all_waveforms_last

def extract_clips_helper2(timeseries, times, clip_size):
    X = readmda(timeseries)
    L=np.shape(X)[1]
    out=np.zeros((np.shape(X)[0],clip_size,len(times)))
    a=int(np.floor((clip_size+1)/2-1))
    del_ind=np.zeros(len(times))>0
    for i in range(len(times)):
        t=times[i]
        t_start=int(t-a)
        t_end=int(t-a+clip_size)
        if t_start<0 or t_end>L:
            del_ind[i]=True
            continue
        out[:,:,i]=X[:,t_start:t_end]
    out=out[:,:,~del_ind]
    return out,del_ind

def get_last_events(firings, num):
    L = firings.shape[1]
    times = firings[1, :]
    labels = firings[2, :]
    K = int(max(labels))
    to_use = np.zeros(L)
    for k in range(1, K + 1):
        inds_k = np.where(labels == k)[0]
        times_k = times[inds_k]
        if (len(times_k) <= num):
            to_use[inds_k] = 1
        else:
            times_k_sorted = np.sort(times_k)
            cutoff = times_k_sorted[len(times_k_sorted) - num]
            to_use[inds_k[np.where(times_k >= cutoff)[0]]] = 1
    return firings[:, np.where(to_use == 1)[0]]

def get_first_events(firings, num):
    L = firings.shape[1]
    times = firings[1, :]
    labels = firings[2, :]
    K = int(max(labels))
    to_use = np.zeros(L)
    for k in range(1, K + 1):
        inds_k = np.where(labels == k)[0]
        times_k = times[inds_k]
        if (len(times_k) <= num):
            # if whole cluster < num spikes, use all the spikes
            to_use[inds_k] = 1
        else:  # otherwise use the first and last num spikes
            times_k_sorted = np.sort(times_k)
            cutoff = times_k_sorted[num]
            to_use[inds_k[np.where(times_k <= cutoff)[0]]] = 1
    return firings[:, np.where(to_use == 1)[0]]

def find_noise_units(metrics_file,noise_overlap_thresh=0.03):
    noise_units=[]
    with open(metrics_file) as metrics_json:
        metrics_data = json.load(metrics_json)
    
    for idx in range(len(metrics_data['clusters'])):
        cur_unit=metrics_data['clusters'][idx]['label']

        noise_overlap = metrics_data['clusters'][idx]['metrics']['noise_overlap']
        if noise_overlap > noise_overlap_thresh:
            noise_units.append(cur_unit)
    return noise_units


def find_bursting_pairs(metrics_file):
    bursting_pairs=[]
   # metrics_file='/home/jojo/RemoteData2/shijie/animals/Jaq/mountainlab_output/20190826/nt18/metrics/metrics_processed_nt18_epoch1.json'
    with open(metrics_file) as metrics_json:
        metrics_data = json.load(metrics_json)
    for idx in range(len(metrics_data['clusters'])):
    # Check if burst parent exists
        if metrics_data['clusters'][idx]['metrics']['bursting_parent']:
            bursting_pairs.append([metrics_data['clusters'][idx]['metrics']['bursting_parent'],metrics_data['clusters'][idx]['label']])
    bursting_pairs=np.array(bursting_pairs)
    return np.array(bursting_pairs)


def find_MUA(all_waveforms_first,all_waveforms_last,sample_num=10000):
    # sample_num: sample number used to

    half_sample_num=int(sample_num/2)
    boot_var_all=[]
    epoch_cluster_all=[]
    for epoch in range(len(all_waveforms_first)):
        print('epoch',epoch)
        K1 = list(all_waveforms_first[epoch].keys())
        if len(K1) == 0:
            continue
        boot_var=[]
        epoch_cluster=[]
        for x1 in K1:
            data1=np.concatenate((all_waveforms_first[epoch][x1],all_waveforms_last[epoch][x1]),axis=2)
            C,T,N1=np.shape(data1)
            n1=np.random.choice(N1,sample_num,replace=True)

            dist11=np.zeros((int(C),int(half_sample_num)))
            for t in range(half_sample_num):
                data1_=np.squeeze(data1[:,:,n1[t]])
                data2_=np.squeeze(data1[:,:,n1[half_sample_num+t]])
                dist11[:,t],_,__=compute_distance_between_clusters_2(data1_,data2_)
            dist11_x1=np.max(np.var(dist11,axis=1))
            boot_var.append(dist11_x1)
            epoch_cluster.append(x1)
        boot_var_all.append(boot_var)
        epoch_cluster_all.append(np.array(epoch_cluster))
    var_all=np.hstack(np.array(boot_var_all))

    threshold=(np.mean(var_all)+np.sqrt(np.var(var_all)))

    MUA_units=[epoch_cluster_all[i][np.argwhere(boot_var_all[i]>threshold)].ravel() for i in range(len(boot_var_all))]

    return MUA_units


def merge_within_epoch(waveforms_first,waveforms_last,K,metrics_file,exclude_pairs):
    #exclude_pairs: do not merge these pairs

    # load metrics file
    with open(metrics_file) as metrics_json:
        metrics_data = json.load(metrics_json)

    # use bootstrap distance to decide
    dmatrix_within,_,__=calculate_dmatrix([waveforms_first],[waveforms_last],[K],np.array([[0,0]]),sample_num=10000)
      
    _,pair_matrix_1=find_pairs_from_matrix(dmatrix_within[:,:,0],f1_adj=0,f2_adj=0,exclude_diag=True)

    # use nearest neighbor to decide
    matrix_overlap=np.zeros_like(pair_matrix_1)
    for idx in range(len(metrics_data['clusters'])):
        cur_unit=metrics_data['clusters'][idx]['label']
        if not np.any(np.isin(K,cur_unit)):
            continue
        overlap = metrics_data['clusters'][idx]['metrics']['overlap_cluster']
        if cur_unit<overlap:
            matrix_overlap[cur_unit-1,overlap-1]+=1
        else:
            matrix_overlap[overlap-1,cur_unit-1]+=1
    
    # do not merge the pairs input from exclude_pair
    if len(exclude_pairs)>0:
        matrix_overlap[exclude_pairs-1]-=1
        matrix_overlap[exclude_pairs[:,1]-1,exclude_pairs[:,0]-1]-=1
    
    # use the intersection of the two methods
    matrix_both=np.multiply(pair_matrix_1,matrix_overlap==1) #reciperical

    pairs_to_merge=np.array([p for p in np.argwhere(matrix_both)])
    
    if len(pairs_to_merge)==0:
        return waveforms_first, waveforms_last,K,np.array([])
    else:
        pairs_to_merge=pairs_to_merge+1

    K_updated=list(set(K)-set(pairs_to_merge[:,1].tolist()))
    for p in range(np.shape(pairs_to_merge)[0]):
        waveforms_first[pairs_to_merge[p,0]]=np.concatenate((waveforms_first[pairs_to_merge[p,0]],waveforms_first[pairs_to_merge[p,1]]),axis=2)
        waveforms_last[pairs_to_merge[p,0]]=np.concatenate((waveforms_last[pairs_to_merge[p,0]],waveforms_last[pairs_to_merge[p,1]]),axis=2)
    
    return waveforms_first,waveforms_last,K_updated,pairs_to_merge

def get_join_matrix(dmatrix,Kmaxes,segment_combos):
    adj_Kmaxes = np.cumsum(Kmaxes)

    pairs_to_merge_BOOT=[]
    for dframe in range(np.shape(segment_combos)[0]):
        print('dframe (bootstrap)',dframe)
        if segment_combos[dframe, 0] == 0:
            f1_adj = 0
        else:
            # otherwise increment cluster numbers based on max of previous
            # segment
            f1_adj = int(adj_Kmaxes[segment_combos[dframe, 0] - 1])
        f2_adj = int(adj_Kmaxes[segment_combos[dframe, 1] - 1])

        pairs_to_merge,_=find_pairs_from_matrix(dmatrix[:, :, dframe],f1_adj,f2_adj)

        pairs_to_merge_BOOT.append(pairs_to_merge)

    return pairs_to_merge_BOOT

def find_pairs_from_matrix(dmatrix,f1_adj=0,f2_adj=0,exclude_diag=False):
    if exclude_diag:
        np.fill_diagonal(dmatrix, np.nan)
    f1_pairs=_nanargmin(dmatrix,AXIS=0)
    f2_pairs=_nanargmin(dmatrix,AXIS=1)
    N_f1, N_f2=np.shape(dmatrix)
    f1_pairs=np.concatenate([np.reshape(f1_pairs,(-1,1)),np.reshape(range(N_f2),(-1,1))],axis=1)
    f2_pairs=np.concatenate([np.reshape(range(N_f1),(-1,1)),np.reshape(f2_pairs,(-1,1))],axis=1)
    f1_pairs=f1_pairs[~np.isnan(f1_pairs[:,0]),:]
    f2_pairs=f2_pairs[~np.isnan(f2_pairs[:,1]),:]

    if np.shape(f1_pairs)[0]>0 and np.shape(f2_pairs)[0]>0:
        f1_pairs_matrix=csc_matrix((np.ones(np.shape(f1_pairs)[0]),(f1_pairs[:,0],f1_pairs[:,1])),shape=[N_f1,N_f2])
        f2_pairs_matrix=csc_matrix((np.ones(np.shape(f2_pairs)[0]),(f2_pairs[:,0],f2_pairs[:,1])),shape=[N_f1,N_f2])
        pairs_matrix=f1_pairs_matrix.multiply(f2_pairs_matrix) # mutual nearest

        pairs_to_merge=np.array([p for p in np.argwhere(pairs_matrix) if dmatrix[p[0],p[1]]<0.9])
        pairs_matrix=pairs_matrix.todense()
        if np.shape(pairs_to_merge)[0]>0:
            pairs_to_merge[:,0]+=f1_adj + 1
            pairs_to_merge[:,1]+=f2_adj + 1
        else:
            pairs_to_merge=[]
    else:
        pairs_to_merge=[]
        pairs_matrix=[]
    return pairs_to_merge,pairs_matrix

def _nanargmin(X,AXIS=1):
    # If all nans in slice, return nan; no axis
    # makes everything nan to start with
    min_tmp = np.zeros(X.shape[1-AXIS])+np.nan 
    # finds the indices where the entire column would be nan, so the nanargmin would raise an error
    d0 = np.nanmin(X, axis=AXIS) 
    # on the indices where we do not have a nan-column, get the right index with nanargmin, and than put the right value in those points
    if AXIS==1:
        min_tmp[~np.isnan(d0)] = np.nanargmin(X[~np.isnan(d0),:], axis=1)
    else:
        min_tmp[~np.isnan(d0)] = np.nanargmin(X[:,~np.isnan(d0)], axis=0)
    return min_tmp